/*
	Client-Server Architecture
		
		is a computing model wherein a server hosts, delivers, and manages resources that a client consumes.

	What is a client?

		A client is an application which creates requests for resources from a server. A client will trigger an action, in a web development context, through a URL and wait for the response of the server.

	What is a server?

		A server is able to host and deliver resources requested by a client. In fact, a single server can handle multiple clients.

	What is Nodejs?

		is an environment to be able to develop applications with Javascript. With this, we can run JS even without the use of an HTML.

		Javascript was originally conceptualized to be used for front-end applications. This is the reason why our vanilla Javascript had to be linked and connected to an HTML file before you can use JS.

		But with advent of Node.js, JS can now be used to create backend applications.

			Note: 
				Front-End is usually the page that we're seeing. It is usually a client application that requests resources from a server or a backend

				Back-end applications are usually server-related applications which handles requests from a client or frontend

		Why is Node.js so popular?

			Performance - Nodejs is one of the most performing environment for creating backend applications for JS

			Familiarity - It uses JS as its language and therefore very familiar for most developers

			NPM - Node Package Manager
				- is the largest registry for node packages. These packages are small bits of program, methods, functions and codes that greatly help in the development of an application.

*/

//console.log("Hello, World");


let http = require("http");

//console.log(http);

http.createServer(function(req,res){
	//http.createServer() method allowed us to create a server and handle the requests of a client.
	//.createServer() method has an anonymous function that handles our clients request and server response. The anonymous function in the createServer method is able to receive two objects, first, req or request, this is the request from the client. Second, is res, this is our server's response. Each request and response parameters are object which contains the details of a request/response as well as methods to handle them.

	//res.writeHead() is a method of the response object. This will allow us to add headers, which are additional information about our server'sresponse. 'Content-Type' is one of the more recognizable headers, it is pertaining to the data type of the content where are responding with. The first argument in writeHead is an HTTP status which is used to tell the client about the status of their request. 200 meaning OK. HTTP 404 - means the resource you're trying to access cannot be found. 403 means the resource youre trying to access is forbidden or requires the proper authentication. 

	//res.end() is a method of the response object which ends the server's response and sends a message/data as a string. Can only be done once and at the end.

	//.listen() is added to allow us to assign a port to a server. There are several taks and processes in our computer which are also designated into their specific ports
		//-port is a virtual point where connections start and end.
		//http://localhost:4000 - localhost is your current machine and 4000 is the port number assigned to where the process/server is listening/running from. This is the port where our server is ran. port 4000 is popularly used for backend applications. port 8000, 5000 these are also available.


	//console.log(req);
	//console.log(req.url)


	if(req.url === "/"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('Hi! My name is what my name is who')
	} else if(req.url === "/login"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('Welcome to the Login Page')
	}

}).listen(4000);

console.log("Server is running on localHost:4000")

/*
	We are now able to run a simple nodejs server. Wherein when we added our URL in the browser, the browser a client actually requested to our server and our server was able to respond with a text.

	We used require() method to load node.js modules.
		A module is a software component or part of a program which contains one or more routines.

		The http module is a default module from node.js

		The http module let nodejs transfer data or let our client and server exchange data via HyperText Transfer Protocol(HTTP).

		protocol => http://localhost:4000 <= server

		With this, the client, which is our browser, automatically created a request and our server was able to respond with a message.

		This is how clients and servers communicate with each other. A client triggers an action from a server via URL and the server responds with the data.

			Note: 
				Messages from a client which triggers an action from a server is called request(req)

				Messages from a server to respond to a clients request is called a response(res)
		
		http module contains methods and other codes which allowed us to create a server and let our client and server communicate through http.

		modules when imported/required are objects
*/

/*
	We can actually respond differently to different requests.

	When our browser requests through server the complete URL is like this:

	http://localhost:4000/ - is called an endpoint.

	We can respond differently to different request URL endpoints
	
*/






